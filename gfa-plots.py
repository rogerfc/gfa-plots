#!/usr/bin/env python

# Plots for Laia
# using Bokeh library - http://bokeh.pydata.org
# usage: ./plot.py data.csv

import plotting
import sys

def main(datafile):
    data = plotting.read(datafile)
    data = plotting.format(data)
    plot = plotting.plot(data, title=datafile)
    plotting.show(plot)


if __name__ == '__main__':
    datafile = sys.argv[-1]
    main(datafile)
