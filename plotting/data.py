import csv
from datetime import datetime

def read(filename):
    data = {}
    with open(filename, 'r') as input:
        csvin = csv.reader(input, delimiter=';')
        rows = [row for row in csvin]
        headers = rows.pop(0)
        for h in headers:
            data[h] = []
        for row in rows:
            for h, v in zip(headers, row):
                data[h].append(v)
    return data

def format(data):
    return {
        'in_temp': [float(v) for v in data['IN_TEMP']],
        'in_rh': [float(v) for v in data['IN_RH']],
        'out_temp': [float(v) for v in data['OUT_TEMP']],
        'out_rh': [float(v) for v in data['OUT_RH']],
        'timestamp': [
            datetime.strptime('{} {}'.format(d, t), '%Y-%m-%d %H:%M:%S')
            for d, t in zip(data['DATE'], data['TIME'])]}
