from bokeh.plotting import figure, gridplot, output_file, show

def plot(data, title):
    output_file("lines.html", title=title)

    t = figure(
        title=title,
        x_axis_type="datetime",
        x_axis_label='timestamp',
        y_axis_label='temperature (ºC)')
    t.line(data['timestamp'], data['in_temp'], legend='IN', line_color="blue")
    t.line(data['timestamp'], data['out_temp'], legend='OUT', line_color="red")

    r = figure(
        title=title,
        x_axis_type="datetime",
        x_axis_label='timestamp',
        y_axis_label='humidity (%)')
    r.line(data['timestamp'], data['in_rh'], legend='IN', line_color="blue")
    r.line(data['timestamp'], data['out_rh'], legend='OUT', line_color="red")

    p = gridplot([[t, r]])

    return p
