# Plots Laia

Small command-line utility to plot GFA data

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need a system with python 3 and virtualenv installed.
Tested on macOS, should run elsewere.

### Installing

Get data from source
```bash
$ git clone https://rogerfc@bitbucket.org/rogerfc/gfa-plots.git
```

Create and activate virtualenv
```bash
$ virtualenv -p python3 ~/.virtualenv/gfa-plots
$ source ~/.virtualenv/plots-laia/bin/activate
(gfa-plots) $
```

Install pip requirements
```bash
(gfa-plots) $ cd gfa-plots
(gfa-plots) gfa-plots $ pip install -r requirements.txt
```

## Plotting

To produce some plot, simply run the plot.py executable against some csv datafile:

```bash
(gfa-plots) gfa-plots $ ./gfa-plots.py test-data/log.gfa3d.extern-2.csv
```

This should open a browser window with the plot in it.

## Built With

* [Bokeh](http://bokeh.pydata.org) - Browser based interactive visualization library

## Authors

* **Roger Firpo** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
